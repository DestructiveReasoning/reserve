#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<fcntl.h>
#include<pthread.h>
#include<semaphore.h>
#include<sys/file.h>
#include<sys/mman.h>

int main() {
	sem_unlink("harwiltz_writeA");
	sem_unlink("harwiltz_readA");
	sem_unlink("harwiltz_writeB");
	sem_unlink("harwiltz_readB");
	shm_unlink("harwiltz-1");
	shm_unlink("harwiltz-1-readers");
	shm_unlink("harwiltz-2");
	shm_unlink("harwiltz-2-readers");
}

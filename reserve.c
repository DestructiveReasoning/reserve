#include<fcntl.h>
#include<semaphore.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/file.h>
#include<sys/mman.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<time.h>

#define NUM_TABLES 10

//Helper struct for storing database attributes
struct db {
	char *data;
	int *readers;
	sem_t *write_lock;
	sem_t *read_lock;
	size_t max_name;
};

//Prompt user for command
int get_command(const char *prompt, char *args[], FILE *src) {
	printf("%s", prompt);
	char *line = NULL;
	char *token;
	int i = 0;
	size_t linecap = 0;
	int length = getline(&line, &linecap, src);
	if(length <= 0) {
		free(line);
		return -1;
	}

	char *line2 = line;

	while((token = strsep(&line2, " \t\n")) != NULL) {
		if(i >= 4) break; //Commands have a maximum of three parameters
		for(unsigned int j = 0; j < strlen(token); j++) {
			if(token[j] <= 32) {
				token[j] = '\0';
			}
		}
		if(strlen(token) > 0) {
			args[i++] = strdup(token);
		}
	}

	free(line);
	return i;
}

//Reset the databases
void dbinit(struct db databaseA, struct db databaseB) {
	sleep(rand() % 10);
	//Wait on both write locks to prevent partial reset
	sem_wait(databaseA.write_lock);
	sem_wait(databaseB.write_lock);
	memset(databaseA.data,0,NUM_TABLES*databaseA.max_name);
	memset(databaseB.data,0,NUM_TABLES*databaseB.max_name);
	//Release both write locks
	sem_post(databaseA.write_lock);
	sem_post(databaseB.write_lock);
}

//Reserve table in database. Reserves first available table if num is -1
int reserve(const char *name, int num, struct db database) {
	if(name == NULL) {
		printf("Invalid name\n");
		return -1;
	}
	int modnum = num % 100; //Handles table numbers in the range 0-9 or {1,2}00-{1,2}09
	if(modnum >= NUM_TABLES) {
		printf("Invalid table number\n");
		return -1;
	}
	unsigned int l = strlen(name);
	if(l == 0 || l >= database.max_name) {
		printf("Name must contain between 1 and %d characters\n", (int) database.max_name);
		return -1;
	}
	//Calculate index of table num
	int hash = (modnum) * database.max_name;
	sleep(rand() % 10);
	//Wait on write lock to prevent multiple simultaneous writers
	sem_wait(database.write_lock);
	if(num < 0) {
		//Find the first available table
		for(num = 0; num < NUM_TABLES; num++) {
			hash = (num)*database.max_name;
			if(database.data[hash] == 0) { //Table is free
				unsigned int c;
				for(c = 0; c < l; c++) {
					database.data[hash + c] = name[c]; //Copy name into database starting at hash
				}
				database.data[hash + c] = 0; //Null terminate
				modnum = num;
				break;
			}
		}
		if(num >= NUM_TABLES) {
			num = -1;
			printf("No tables available for %s in this section!\n", name);
		}
	} else {
		if(database.data[hash] != 0) { //Table is reserved
			printf("Table %d already reserved! %s doesn't get a reservation.\n", num, name);
			sem_post(database.write_lock); //Release writer lock before returning
			return -1;
		}
		else {
			unsigned int c;
			for(c = 0; c < l; c++) {
				database.data[hash + c] = name[c]; //Copy name into database starting at hash
			}
			database.data[hash + c] = 0; //Null terminate
		}
	}
	//Release the write lock to allow subsequent writers to write
	sem_post(database.write_lock);
	return modnum;
}

//Check the status of the databases
void status(struct db databaseA, struct db databaseB) {
	sleep(rand() % 10);
	sem_wait(databaseA.read_lock); //Get exclusive access to databaseA.readers
	(*databaseA.readers)++; //Increment database A's reader count, as process is about to read from db
	if((*databaseA.readers) == 1) //If first reader, wait on write lock. 
		sem_wait(databaseA.write_lock);
	sem_post(databaseA.read_lock); //Otherwise, don't, because if no one is writing then there can be infinitely many readers
	printf("=========================\n");
	printf("Section A\n");
	int c;
	for(c = 0; c < NUM_TABLES; c++) {
		printf("%d: ", 100 + c);
		if(databaseA.data[c * databaseA.max_name] == 0) { //Indicates no reservation
			printf("Open for reservation\n");
		} else {
			printf("%s\n", databaseA.data + c * databaseA.max_name);
		}
	}
	sem_wait(databaseA.read_lock); //Get exclusive access to databaseA.readers
	(*databaseA.readers)--; //Decrement database A's reader count, as this process is finished reading from db
	if((*databaseA.readers) == 0) //If no more readers left, allow writers to write
		sem_post(databaseA.write_lock);
	sem_post(databaseA.read_lock); //Otherwise, don't, because if people are reading no one should be writing

	//Same procedure as for database A
	sem_wait(databaseB.read_lock);
	(*databaseB.readers)++;
	if(*databaseB.readers == 1)
		sem_wait(databaseB.write_lock);
	sem_post(databaseB.read_lock);
	printf("\nSection B\n");
	for(c = 0; c < NUM_TABLES; c++) {
		printf("%d: ", 200 + c);
		if(databaseB.data[c * databaseB.max_name] == 0) {
			printf("Open for reservation\n");
		} else {
			printf("%s\n", databaseB.data + c * databaseB.max_name);
		}
	}
	printf("=========================\n");
	sem_wait(databaseB.read_lock);
	(*databaseB.readers)--;
	if(*databaseB.readers == 0)
		sem_post(databaseB.write_lock);
	sem_post(databaseB.read_lock);
}

//Parses and handles user command line
void parse_command(int argc, char *argv[], struct db databaseA, struct db databaseB) {
	if(argc <= 0) return;
	if(strcmp(argv[0], "exit") == 0) {
		//Make sure to close semaphores
		sem_close(databaseA.write_lock);
		sem_close(databaseB.write_lock);
		for(int c = 0; c < argc; c++) {
			free(argv[c]); //Free allocated args strings
		}
		exit(0);
	}
	if(strcmp(argv[0], "reserve") == 0) {
		if(argc < 3) {
			printf("Invalid format.\nUSAGE: reserve name section [table number]\n");
			return;
		}
		int tablenumber = (argc == 3) ? -1 : atoi(argv[3]); //If no table number is specified, indicate -1 to find first table
		struct db chosen;
		int section;
		if(strcmp(argv[2], "A") == 0) {
			if(tablenumber >= 200) {
				printf("Invalid table number for section A\n");
				return;
			}
			section = 0;
			chosen = databaseA;
		}
		else if(strcmp(argv[2], "B") == 0) {
			if(tablenumber < 200 && tablenumber >= 100) {
				printf("Invalid table number for section B\n");
				return;
			}
			section = 1;
			chosen = databaseB;
		}
		else {
			printf("Invalid section: %s\n", argv[2]);
			return;
		}
		tablenumber = reserve(argv[1], tablenumber, chosen);
		if(tablenumber >= 0) {
			printf("Table %d reserved for %s\n", (section+1)*100 + tablenumber, argv[1]);
		}
	}
	else if (strcmp(argv[0], "status") == 0) {
		status(databaseA, databaseB);
	}
	else if (strcmp(argv[0], "init") == 0) {
		dbinit(databaseA, databaseB);
	}
}

//Run commands from a file
void run_file(const char *path, struct db databaseA, struct db databaseB) {
	FILE *f = fopen(path, "r");
	if(f == NULL) {
		printf("File not found or could not be opened: %s\n", path);
		exit(1);
	}
	char *args[4];
	int argc;
	while((argc = get_command("",args,f)) >= 0) { //Call get_command on file stream rather than stdin
		parse_command(argc, args, databaseA, databaseB);
		for(int c = 0; c < argc; c++) {
			free(args[c]);
		}
	}
	fclose(f);
	exit(0);
}

int main(int argc, char *argv[]) {
	time_t now; //For random sleep, for testing purposes
	srand(time(&now));
	const size_t region_size = sysconf(_SC_PAGE_SIZE); //Max page size, to use for database size
	const size_t max_name = region_size / (NUM_TABLES);
	sem_t *writeLockA = sem_open("harwiltz_writeA", O_RDWR|O_CREAT, 00666, 1); //Mutex for writing to database A
	sem_t *writeLockB = sem_open("harwiltz_writeB", O_RDWR|O_CREAT, 00666, 1); //Mutex for writing to database B
	sem_t *readLockA = sem_open("harwiltz_readA", O_RDWR|O_CREAT, 00666, 1); //Mutex for modifying database A's reader count
	sem_t *readLockB = sem_open("harwiltz_readB", O_RDWR|O_CREAT, 00666, 1); //Mutex for modifying database B's reader count
	if(writeLockA == SEM_FAILED) perror("Write Semaphore A creation");
	if(writeLockB == SEM_FAILED) perror("Write Semaphore B creation");
	if(readLockA == SEM_FAILED) perror("Read Semaphore A creation");
	if(readLockA == SEM_FAILED) perror("Read Semaphore B creation");

	char *addrA, *addrB;
	int *readersA, *readersB;
	int fdA = shm_open("harwiltz-1", O_RDWR|O_CREAT|O_EXCL, 00666); //Shared memory object for database A data
	if(fdA == -1) { //shared object already existed
		fdA = shm_open("harwiltz-1", O_RDWR|O_CREAT, 00666);
		addrA = mmap(NULL, region_size, PROT_READ|PROT_WRITE, MAP_SHARED, fdA, 0);
	} else { //shared object is new
		if(ftruncate(fdA, region_size) != 0) { //Truncate to size region_size
			printf("Truncation error\n");
			exit(1);
		}
		addrA = mmap(NULL, region_size, PROT_READ|PROT_WRITE, MAP_SHARED, fdA, 0);
		memset(addrA, 0, region_size);
	}
	int fdrA = shm_open("harwiltz-1-reader", O_RDWR|O_CREAT|O_EXCL, 00666); //Shared memory object for database A reader count
	if(fdrA == -1) {
		fdrA = shm_open("harwiltz-1-reader", O_RDWR|O_CREAT, 00666);
		readersA = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, fdrA, 0);
	} else {
		if(ftruncate(fdrA, sizeof(int)) != 0) { //Truncate to the size of an int
			printf("Truncation error\n");
			exit(1);
		}
		readersA = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, fdrA, 0);
		memset(readersA, 0, sizeof(int));
	}
	int fdB = shm_open("harwiltz-2", O_RDWR|O_CREAT|O_EXCL, 00666); //Shared memory object for database B data
	if(fdB == -1) {
		fdB = shm_open("harwiltz-2", O_RDWR|O_CREAT, 00666);
		addrB = mmap(NULL, region_size, PROT_READ|PROT_WRITE, MAP_SHARED, fdB, 0);
	} else {
		if(ftruncate(fdB, region_size) != 0) {
			printf("Truncation error\n");
			exit(1);
		}
		addrB = mmap(NULL, region_size, PROT_READ|PROT_WRITE, MAP_SHARED, fdB, 0);
		memset(addrB, 0, region_size);
	}
	int fdrB = shm_open("harwiltz-2-reader", O_RDWR|O_CREAT|O_EXCL, 00666); //Shared memory object for database B reader count
	if(fdrB == -1) {
		fdrB = shm_open("harwiltz-2-reader", O_RDWR|O_CREAT, 00666);
		readersB = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, fdrB, 0);
	} else {
		if(ftruncate(fdrB, sizeof(int)) != 0) {
			printf("Truncation error\n");
			exit(1);
		}
		readersB = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, fdrB, 0);
		memset(readersB, 0, sizeof(int));
	}
	if(fdA == -1) perror("SHM error");
	if(fdB == -1) perror("SHM error");

	close(fdA);
	close(fdB);
	close(fdrA);
	close(fdrB);
	struct db databaseA = {addrA, readersA, writeLockA, readLockA, max_name};
	struct db databaseB = {addrB, readersB, writeLockB, readLockB, max_name};

	int processes[20]; //Stores PID's of forked processes
	//Run at most 20 input commands simultaneously, for easier testing than sleeping
	for(int c = 1; c < argc; c++) {
		int pid = fork();
		if(pid == 0) {
			run_file(argv[c], databaseA, databaseB);
			exit(0);
		} else if (pid > 0) {
			processes[(c-1)%20] = pid;
		}
	}
	for(int c = 0; c < argc - 1; c++) {
		//This makes the shell prompt nicer and easier to read after a fork
		waitpid(processes[c], 0, 0); //Wait for files to finish executing before taking more command lines
	}

	char *args[4];
	const char *prompt = "reserve > ";
	int argcount;
	while(1) { //Take user command line
		argcount = get_command(prompt, args, stdin);
		parse_command(argcount, args, databaseA, databaseB);
		for(int c = 0; c < argcount; c++) {
			free(args[c]);
		}
	}

	for(unsigned int c = 0; c < 4; c++) {
		free(args[c]);
	}
	free(args);

	//Clean up
	sem_close(writeLockA);
	sem_close(writeLockB);
	sem_close(readLockA);
	sem_close(readLockB);
	munmap(addrA, region_size);
	munmap(addrB, region_size);
	munmap(readersA, region_size);
	munmap(readersB, region_size);
	free(addrA);
	free(addrB);
	free(readersA);
	free(readersB);
	return 0;
}

res: 
	gcc -pthread -Wall -Wextra -Wshadow -Wunreachable-code -pedantic reserve.c -o reserve -lrt
	gcc -pthread -Wall -Wextra -Wshadow -Wunreachable-code -pedantic clear.c -o clear -lrt

all: res

memtest: res
	valgrind --trace-children=yes --leak-check=yes --leak-check=full --show-leak-kinds=all ./reserve

memtest-conc: res
	valgrind --trace-children=yes --leak-check=yes --leak-check=full --show-leak-kinds=all ./reserve cmd1 cmd2 cmd3
